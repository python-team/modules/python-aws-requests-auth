python-aws-requests-auth (0.4.3-5) unstable; urgency=medium

  * Team upload.
  * Really drop dependency on python3-mock

 -- Alexandre Detiste <tchet@debian.org>  Wed, 18 Dec 2024 14:42:49 +0100

python-aws-requests-auth (0.4.3-4) unstable; urgency=medium

  * Team upload.
  * Drop dependency on python3-mock
  * Set "Rules-Requires-Root: no"
  * Use dh-sequence-python3

  [ Sébastien Delafond ]
  * d/control: remove Freexian team members from Uploaders

 -- Alexandre Detiste <tchet@debian.org>  Sat, 14 Dec 2024 13:15:39 +0100

python-aws-requests-auth (0.4.3-3) unstable; urgency=medium

  [ Neil Williams ]
  * Team upload.
  * Add d/gbp.conf to handle default debian/master branch
  * Fix dh_python depends (lintian error)
  * Add Salsa CI
  * Update standards version

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 23 Nov 2022 22:33:55 +0000

python-aws-requests-auth (0.4.3-2) unstable; urgency=medium

  * Team upload.
  * Run upstream tests during build time.
    - and thus add BD on python3-{mock,botocore}.

 -- Utkarsh Gupta <utkarsh@debian.org>  Sat, 15 May 2021 20:16:59 +0530

python-aws-requests-auth (0.4.3-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sophie Brun ]
  * New upstream version 0.4.3
  * Bump Standards-Version to 4.5.1 (no changes)
  * Bump debhelper-compat to 13

 -- Sophie Brun <sophie@freexian.com>  Thu, 21 Jan 2021 10:13:15 +0100

python-aws-requests-auth (0.4.1-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Tue, 27 Aug 2019 07:51:18 +0200

python-aws-requests-auth (0.4.1-1) unstable; urgency=medium

  * Initial release (Closes: #879745)

 -- Sophie Brun <sophie@freexian.com>  Tue, 24 Oct 2017 16:56:07 +0200
